import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: nuevo(),
    );
  }

  Widget nuevo() {
    return Scaffold(
      body: Container(
        color: Colors.indigo,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            cuerpo('Find friends', 'Groups', icono(), icono1()),
            cuerpo('Marketplace', 'Videos on Watch', icono2(), icono3()),
            cuerpo('Events', 'Memories', icono4(), icono5()),
            cuerpo('Find friends', 'Groups', icono(), icono1()),
            cuerpo('Marketplace', 'Videos on Watch', icono2(), icono3()),
          ],
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title: Text(
          "Menu",
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
        actions: <Widget>[
          Icon(
            Icons.search,
            color: Colors.white,
            size: 35,
          ),
        ],
        elevation: 0.0,
      ),
    );
  }

  Widget cuerpo(String titulo1, String titulo2, Widget ico1, Widget ico2) {
    return Container(
      height: 105,
      width: 500,
      color: Colors.indigo,
      margin: EdgeInsets.only(top: 15, right: 10, left: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          caja(titulo1, ico1),
          caja(titulo2, ico2),
        ],
      ),
    );
  }

  Widget caja(String titulo, Widget ico) {
    return Container(
      height: 105,
      width: 160,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 10, color: Colors.white),
          borderRadius: BorderRadius.circular(5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ico,
          texto(titulo),
        ],
      ),
    );
  }

  Widget texto(String titulo) {
    return Container(
      child: Text(titulo),
    );
  }

  Widget icono() {
    return Container(
      child: Icon(Icons.search, color: Colors.red, size: 35),
    );
  }

  Widget icono1() {
    return Container(
      child: Icon(Icons.group, color: Colors.green, size: 35),
    );
  }

  Widget icono2() {
    return Container(
      child: Icon(
        Icons.shopping_cart, 
        color: Colors.purple, size: 35),
    );
  }

  Widget icono3() {
    return Container(
      child: Icon(Icons.videocam, color: Colors.orange, size: 35),
    );
  }

  Widget icono4() {
    return Container(
      child: Icon(Icons.location_on, color: Colors.teal, size: 35),
    );
  }

  Widget icono5() {
    return Container(
      child: Icon(Icons.access_time, color: Colors.grey, size: 35),
    );
  }
}
